import com.m_systems.trapi.lib.*;
import com.reuters.adt.trapi.event.*;
import com.m_systems.trapi.lib.record.*;
import com.m_systems.comms.*;

public class TestLogin implements OrderListener, LOMSTradingListener
{
    private String host="localhost";
    private int port=8089;
    private String username="exue.proxy.client.1";
    private String password="d";

    private TradingEngine engine;
    private LOMSTradingEngine engineLOMS;

    private void connectRFQEngine()
    {
        CConnectionInfo connInfo=new CConnectionInfo(CConnectionType.DIRECT_SCS,
                                                     "GID.orders",
                                                     host,
                                                     port,
                                                     CTransferProtocol.STANDARD,
                                                     10,
                                                     CEncryptionType.NONE);

        CUserInfo userInfo=new CUserInfo(username, password);
        System.out.println("Connecting to RFQ as " + username + "...");

        try
        {
            engine.Connect(this, connInfo, userInfo);
            System.out.println("RFQ connected");
        }
        catch (TradingException ex)
        {
            System.out.println("RFQ exception: " + ex);
        }
    }

    private void connectLOMSEngine()
    {
        CConnectionInfo connInfo=new CConnectionInfo(CConnectionType.DIRECT_SCS,
                                                     "BT.Server.LOMS",
                                                     host,
                                                     port,
                                                     CTransferProtocol.STANDARD,
                                                     10,
                                                     CEncryptionType.NONE);

        CUserInfo userInfo=new CUserInfo(username, password);

        try
        {
            System.out.println("Connecting to LOMS as " + username + "...");
            engineLOMS.connect(this, connInfo, userInfo);
            System.out.println("LOMS connected");
        }
        catch (TradingException ex)
        {
            System.out.println("LOMS exception: " + ex);
        }
    }

    private void start()
    {
        TradingLibrary.Init();
        engine=new TradingEngine(true);
        engineLOMS=new LOMSTradingEngine(true);

        connectRFQEngine();
        connectLOMSEngine();

        engine.Disconnect();
        engine.close();
        engineLOMS.disconnect();
        engineLOMS.close();
    }

    public static void main(String args[])
    {
        new TestLogin().start();
    }

    @Override
    public void logFailed(OrderEvent event, EventFailure cause) {}
    @Override
    public void logPassed(OrderEvent event) {}
    @Override
    public void logPersisted(OrderEvent event) {}
    @Override
    public void orderConfirmed(OrderEvent event) {}
    @Override
    public void orderHeld(OrderEvent event, EventFailure cause) {}
    @Override
    public void orderObserverRecieved(OrderEvent event) {}
    @Override
    public void orderPickedUp(OrderEvent event) {}
    @Override
    public void quotationDenied(OrderEvent event, EventFailure cause) {}
    @Override
    public void quotationReceived(OrderEvent event) {}
    @Override
    public void quotationTimeout(OrderEvent event) {}
    @Override
    public void quotationWindowExpired(OrderEvent event) {}
    @Override
    public void quotationWithdrawn(OrderEvent event) {}
    @Override
    public void submissionFailed(OrderEvent event, EventFailure cause) {}
    @Override
    public void submissionPassed(OrderEvent event) {}
    @Override
    public void onDisconnect() {}
    @Override
    public void onOrderActivate(String userId, String gtmsId) {}
    @Override
    public void onOrderActivateFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onOrderCancel(String userId, String gtmsId) {}
    @Override
    public void onOrderCancelFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onOrderCancelPass(String userId, String gtmsId) {}
    @Override
    public void onOrderDeactivate(String userId, String gtmsId) {}
    @Override
    public void onOrderDeactivateFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onOrderDelete(String userId, String gtmsId) {}
    @Override
    public void onOrderDeleteFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onOrderEvent(String userId, String gtmsId, LOMSOrderEvent oOrderEvent) {}
    @Override
    public void onOrderLock(String userId, String gtmsId, String lockOwner) {}
    @Override
    public void onOrderLockFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onOrderModify(String userId, String gtmsId, LOMSOrderModification modify) {}
    @Override
    public void onOrderModifyFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onOrderRegister(String userId, String gtmsId) {}
    @Override
    public void onOrderRegisterFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onOrderUnlock(String userId, String gtmsId) {}
    @Override
    public void onOrderUnlockFail(String userId, String gtmsId, String reason) {}
    @Override
    public void onSelectProxiedUser(String proxiedUser) {}
    @Override
    public void onSelectProxiedUserFail(String proxiedUser, String reason) {}
}