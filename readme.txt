
javac -cp guava-19.0.jar:TrAPIlib.jar:commons-cli-1.4.jar:. TradingEngineLogin.java

java -cp guava-19.0.jar:TrAPIlib.jar:commons-cli-1.4.jar:. TradingEngineLogin -h localhost -p 8080 -u exue.proxy.client -pa d

Sample successful connection output:
**********************************************************************
TrAPI - Trading Application Interface Library: TrAPI-3.5.188.BD
TrAPI   using JavaLib: 3.5.1209 (Core: 3.5.21.10)
TrAPI   using PrAPI: 2.0.45
TrAPI   using native JCL comms: 3.5.21.10
TrAPI   using Licence Handler: 23
TrAPI   using API Common: 2.0.77

** INFO ** Using System Properties
** INFO ** Using com.m_systems.trapi.log.enable:[false]
** INFO ** Using com.m_systems.trapi.proxy_load_account_details:[true]
** INFO ** Using com.m_systems.trapi.callback_threads:[1]
** INFO ** Using com.m_systems.trapi.log.enable:[false]
Connecting to RFQ as exue.proxy.client.1...
** INFO ** TrAPI licence - OK : Expiry date: Aug 5, 2020 (8 days left)
** INFO ** Connected to a GID (ET 2.4) system
RFQ connected
**********************************************************************