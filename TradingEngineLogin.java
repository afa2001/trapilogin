import com.m_systems.trapi.lib.*;
import com.reuters.adt.trapi.event.*;
import com.m_systems.comms.*;
import org.apache.commons.cli.*;

public class TradingEngineLogin implements OrderListener
{

    private TradingEngine engine;

    private void connectRFQEngine(String strHost, int iPort, String strUsername, String strPassword)
    {
        CConnectionInfo connInfo=new CConnectionInfo(CConnectionType.DIRECT_SCS,
                                                     "GID.orders",
                                                     strHost,
                                                     iPort,
                                                     CTransferProtocol.STANDARD,
                                                     10,
                                                     CEncryptionType.NONE);

        CUserInfo userInfo=new CUserInfo(strUsername, strPassword);
        System.out.println("Connecting to RFQ as " + strUsername + "...");

        try
        {
            engine.Connect(this, connInfo, userInfo);
            System.out.println("RFQ connected");
        }
        catch (TradingException ex)
        {
            System.out.println("RFQ exception: " + ex);
        }
    }

    private void start(String strHost, String strPort, String strUsername, String strPassword)
    {
        TradingLibrary.Init();
        engine=new TradingEngine(true);
        
        connectRFQEngine(strHost, Integer.parseInt(strPort), strUsername, strPassword);

        engine.Disconnect();
        engine.close();
        
    }

    public static void main(String args[])
    {
        //command line argument parsing
        Options options = new Options();

        Option host = new Option("h", "host", true, "hostname/ip address");
        host.setRequired(true);
        options.addOption(host);

        Option port = new Option("p", "port", true, "port");
        port.setRequired(true);
        options.addOption(port);

        Option username = new Option("u", "username", true, "et proxy client user name");
        username.setRequired(true);
        options.addOption(username);

        Option password = new Option("pa", "password", true, "password");
        password.setRequired(true);
        options.addOption(password);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("command line arguments:", options);

            System.exit(1);
        }

        String strHost = cmd.getOptionValue("h").trim();
        String strPort = cmd.getOptionValue("p").trim();
        String strUsername = cmd.getOptionValue("u").trim();
        String strPassword = cmd.getOptionValue("pa").trim();

        System.out.println(strHost);
        System.out.println(strPort);
        System.out.println(strUsername);
        System.out.println(strPassword);


        //now starts initialization and connect to ET
        new TradingEngineLogin().start(strHost, strPort, strUsername, strPassword);
    }

    @Override
    public void logFailed(OrderEvent event, EventFailure cause) {}
    @Override
    public void logPassed(OrderEvent event) {}
    @Override
    public void logPersisted(OrderEvent event) {}
    @Override
    public void orderConfirmed(OrderEvent event) {}
    @Override
    public void orderHeld(OrderEvent event, EventFailure cause) {}
    @Override
    public void orderObserverRecieved(OrderEvent event) {}
    @Override
    public void orderPickedUp(OrderEvent event) {}
    @Override
    public void quotationDenied(OrderEvent event, EventFailure cause) {}
    @Override
    public void quotationReceived(OrderEvent event) {}
    @Override
    public void quotationTimeout(OrderEvent event) {}
    @Override
    public void quotationWindowExpired(OrderEvent event) {}
    @Override
    public void quotationWithdrawn(OrderEvent event) {}
    @Override
    public void submissionFailed(OrderEvent event, EventFailure cause) {}
    @Override
    public void submissionPassed(OrderEvent event) {}
    
}